var $ = require('jquery')

$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
})

module.exports = function(nombre, correo, telefono, mensaje, tipo, callback) {
    $.ajax({
        data: {
            nombre: nombre,
            correo: correo,
            telefono: telefono,
            mensaje: mensaje,
            tipo: tipo
        },
        url:'enviar_formulario',
        type:'POST',
        success: callback
    });
}
