var $ = require('jquery');
var scroll = require('scroll')
var page = require('scroll-doc')()
var enviarForm = require('./formulario.js')

// nl2br polyfill
String.prototype.nl2br = function()
{
    return this.replace(/\n/g, "<br />");
}

// Extend String class to support JS email validation as per
// https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
String.prototype.isEmail = function()
{
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.toLowerCase());
}

$(function() {

    window.reservaChangeHandler = function() {
        var activeOverlay = $('.overlay.active');
        var validation = activeOverlay.data('validation');
        var result = window[validation].call();
        if (result != "OK") {
            $('.errors', activeOverlay).html(result.nl2br()).css({opacity: 1});
            $('button', activeOverlay).attr('disabled', 'disabled');
        } else {
            $('button', activeOverlay).attr('disabled', null);
            $('.errors', activeOverlay).empty().css({opacity: 0});
        }
        return result;
    }

    window.validarPlus = function()
    {
        var errors = [];
        if($('#plus-name').val().trim() == '') {
            errors.push('El nombre no puede estar vacío');
        }
        if($('#plus-phone').val().trim() == '') {
            errors.push('El teléfono no puede estar vacío');
        }
        if(!$('#plus-email').val().isEmail()) {
            errors.push('El email no parece válido');
        }
        if (errors.length == 0) {
            return 'OK';
        }
        return errors.join("\n");
    }


    $('[name=llave]').change(function() {
        llave();
        validarReserva();
        calcularReserva();
    });

    $('#banner .button').click(function() {
        scroll.top(page, $('#servicios').position().top - $('#header').height());
        $('#servicios header').removeClass('focustext');
        window.setTimeout(function() { $('#servicios header').addClass('focustext') }, 0);
        return false;
    });
    $('.open-nav').click(function() {
        $('.overlay').removeClass('active');
        if ($(this).hasClass('to-go')) {
            $('#myNav').addClass('active');
        } else {
            $('#plusNav').addClass('active');
        }
        openNav();
    });
    $('#myNav select, #myNav input').change(reservaChangeHandler);
    $('#plusNav select, #plusNav input').change(reservaChangeHandler);
    $('#GuardarReservaPlus').click(function() {
        if (reservaChangeHandler() == 'OK') {
            enviarForm(
                $('#plus-name').val(),
                $('#plus-email').val(),
                $('#plus-phone').val(),
                $('#plus-msg').val(),
                'PLUS',
                (response) => {  
                    closeNav()
                    swal('Formulario Enviado!','Nos pondremos en contacto con tigo a la brevedad.','success');
                    //pagos(response)
                }
            );
        }
    });
});
