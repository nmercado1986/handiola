var enviarForm = require('./formulario.js');
var $ = require('jquery')

$(function() {
    $('#enviarForm').click(() => {
        enviarForm(
            $('#nombre').val(),
            $('#correo').val(),
            $('#telefono').val(),
            $('#mensaje').val(),
            '',
            (response) => {
                swal('Formulario Enviado!','Nos pondremos en contacto con tigo a la brevedad.','success');
            }
        );
    });
})
 

